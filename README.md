# SpringRESTExercise

## Name
Spring REST Exercise

## Description
This program application contains the CRUD methods of a server. This program allows the user to add, edit, delete,filter, and search a list of employees.


## Installation
Have Postman, InteliJ and an up to date JDK file downloaded. Open the DemoRunner file in Intelij and run the file. SpringBoot should run in the console. Open Postman and import the attached JSON file titled "Spring Rest API Exercise collection" from the folder. Once the queries are imported, indiviaually select each query and click Send or auto run the collection in order. 

## Usage
Examples

## Support
If help is needed, please contact support or D.I. von Briesen directly by smoke signals.

## Roadmap
Assignment is completed unless specified.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Created by Richy Phongsavath and the rest of the CO-CP Alpha group.

## License
For open source projects, say how it is licensed.

## Project status
Assignment is completed unless stated otherwise.
